#!/bin/bash
# n=1
# while [ $n -le 10 ]
# do
# 	echo $n #mostrar valor de n
# 	sleep 1
# 	clear
# 	((n++)) #agregarle el numero a n
# done	


n=1
for n in {1..10} #para n en una secuencia del 1 al 10
do
	echo $n #mostrar valor de n
	sleep 1
	clear
	((n++)) #agregarle el numero a n
done